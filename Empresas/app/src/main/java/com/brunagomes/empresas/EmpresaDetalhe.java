package com.brunagomes.empresas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class EmpresaDetalhe extends AppCompatActivity {

    private ImageView imageEmpresa;
    private TextView textDetalhe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empresa_detalhe);

        imageEmpresa = findViewById(R.id.imageEmpresa);
        textDetalhe = findViewById(R.id.textDetalhe);
    }
}

package com.brunagomes.empresas;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class Pesquisa extends AppCompatActivity {

    private ImageView imagePesquisa;
    private ImageView imageFecha;
    private TextInputLayout textInputPesquisa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pesquisa);

        imagePesquisa = findViewById(R.id.imagePesquisa);
        imageFecha = findViewById(R.id.imageFecha);
        textInputPesquisa = findViewById(R.id.textInputPesquisa);
    }
}
